﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerlinNoise : MonoBehaviour
{

    public int mapWidth, mapHeight, seed, octaves;
    public float scale, persistance, lacunarity;
    public Vector2 offset;
    float[,] noiseMap;

    public void updateMaterial()
    {
    }
}

