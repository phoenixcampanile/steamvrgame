﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class NoiseData : UpdatableData
{
    public Noise.NormalizeMode normalizeMode;
    public int octaves, seed;
    public float scale, lacunarity;
    [Range(0, 1)]
    public float persistance;
    public Vector2 offset;

#if UNITY_EDITOR
    protected override void OnValidate()
    {
        if (lacunarity < 1) lacunarity = 1;
        if (octaves < 0) octaves = 0;
        if (scale < 0.0001f) scale = 0.0001f;

        base.OnValidate();
    }
#endif
}
