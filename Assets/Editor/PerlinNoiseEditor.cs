﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(PerlinNoise))]
public class PerlinNoiseEditor : Editor
{

    public override void OnInspectorGUI()
    {
        PerlinNoise perlinNoise = (PerlinNoise)target;

        perlinNoise.mapWidth = EditorGUILayout.IntField("Map Width", perlinNoise.mapWidth);
        perlinNoise.mapHeight = EditorGUILayout.IntField("Map Height", perlinNoise.mapHeight);
        if (perlinNoise.mapWidth < 1) perlinNoise.mapWidth = 1;
        if (perlinNoise.mapHeight < 1) perlinNoise.mapHeight = 1;
        perlinNoise.scale = EditorGUILayout.FloatField("Scale", perlinNoise.scale);
        if (perlinNoise.scale < 0)perlinNoise.scale = 0.0001f;
        perlinNoise.octaves = EditorGUILayout.IntField("Octaves", perlinNoise.octaves);
        perlinNoise.persistance = EditorGUILayout.Slider("Persistance", perlinNoise.persistance,0,1);
        perlinNoise.lacunarity = EditorGUILayout.FloatField("Lacunarity", perlinNoise.lacunarity);
        if (perlinNoise.lacunarity < 1) perlinNoise.lacunarity = 1;
        if (perlinNoise.octaves < 0) perlinNoise.octaves = 0;
        perlinNoise.offset = EditorGUILayout.Vector2Field("Offset", perlinNoise.offset);
        perlinNoise.seed = EditorGUILayout.IntField("Seed", perlinNoise.seed);


        if (GUILayout.Button("Refresh"))
        {
            perlinNoise.updateMaterial();
        }
    }



}
