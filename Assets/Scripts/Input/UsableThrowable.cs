﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class UsableThrowable : Throwable
{
    [Tooltip("The local point which acts as a positional and rotational offset to use while held with a grip type grab")]
    public Transform gripOffset;

    [Tooltip("The local point which acts as a positional and rotational offset to use while held with a pinch type grab")]
    public Transform pinchOffset;



    protected override void HandHoverUpdate(Hand hand)
    {
        GrabTypes startingGrabType = hand.GetGrabStarting();

        if (startingGrabType != GrabTypes.None)
        {
            if (startingGrabType == GrabTypes.Grip)
            {
                hand.AttachObject(gameObject, startingGrabType, attachmentFlags, gripOffset);

            }

            hand.HideGrabHint();
        }
    }
    protected override void HandAttachedUpdate(Hand hand)
    {
        if (interactable.skeletonPoser != null)
        {
            //interactable.skeletonPoser.SetBlendingBehaviourEnabled("GunPose (MAIN)", hand.currentAttachedObjectInfo.Value.grabbedWithType == GrabTypes.Pinch);
        }

        base.HandAttachedUpdate(hand);
    }

}
