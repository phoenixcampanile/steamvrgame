﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class Gun : MonoBehaviour
{

    public AudioClip soundEffect = null;
    public GameObject ray;
    public bool canFire = true;
    public bool attached = false;
    public SteamVR_Action_Boolean fire = null;


    // Start is called before the first frame update
    void Start()
    {
        
    }


    // Update is called once per frame
    void Update()
    {
        if (attached&& fire.stateDown&&canFire)
        {
            Debug.Log("PIAA");
            Shoot();
            canFire = false;
        }else
        {
            canFire = true;
        }
    }
    
    void Shoot()
    {
        RaycastHit hit;
        // Does the ray intersect any objects excluding the player layer
        if (Physics.Raycast(ray.transform.position, ray.transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity))
        {
            Debug.Log("Hit, "+hit.transform.gameObject.name);
            if (hit.transform.gameObject.tag == "target")
            {
                GameObject.Destroy(hit.transform.gameObject);
            }
        }
        else
        {
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.white);
            Debug.Log("Did not Hit");
        }
        if (soundEffect != null)
        {
            GameObject source = new GameObject();
            source.AddComponent<AudioSource>();
            source.AddComponent<TimedDeath>();
            source.GetComponent<TimedDeath>().lifeTime = soundEffect.length;
            source.transform.position = transform.position;
            source.GetComponent<AudioSource>().PlayOneShot(soundEffect);
        }


    }
    public void setAttached(bool a)
    {
        this.attached = a;
    }

}
