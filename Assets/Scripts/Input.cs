﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class Input : MonoBehaviour
{

    public SteamVR_Action_Boolean Grip;
    public SteamVR_Action_Single Squeeze;
    public SteamVR_Action_Boolean Trigger;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        CheckGrip();
        CheckSqueeze();
        CheckTrigger();
    }

    void CheckGrip()
    {
        if (Grip.stateDown)
        {
            //Debug.Log("Gripping");
        }
        if (Grip.stateUp)
        {
            //Debug.Log("NOt Gripping");
        }
    }

    void CheckSqueeze()
    {
        //Debug.Log(Squeeze.axis);
    }

    void CheckTrigger()
    {
        if (Trigger.stateDown)
        {
            //Debug.Log("Trigger Down");
        }
        if (Trigger.stateUp)
        {
            //Debug.Log("Trigger UP");
        }
    }
}