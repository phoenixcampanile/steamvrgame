﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedDeath : MonoBehaviour
{
    public float lifeTime = 0;
    private float aliveTime = 0;
    private float startTime = 0;
    // Start is called before the first frame update
    void Start()
    {
        startTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > startTime + lifeTime) GameObject.Destroy(gameObject);
        aliveTime++;
    }
}
