﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class SimpleCarController : MonoBehaviour
{
    public SteamVR_Action_Vector2 Throttle = null;
    public Transform steeringWheel = null;

    public List<AxleInfo> axleInfos; // the information about each individual axle
    public float maxMotorTorque; // maximum torque the motor can apply to wheel
    public float maxSteeringAngle; // maximum steer angle the wheel can have

    

    public void FixedUpdate()
    {

        float motor = maxMotorTorque * UnityEngine.Input.GetAxis("Vertical");
        float steeringa = maxSteeringAngle * UnityEngine.Input.GetAxis("Horizontal");

        float steering = steeringWheel.gameObject.GetComponent<LinearMapping>().value;
        float wheelRotation = steeringWheel.localEulerAngles.y;
        if (wheelRotation > 180)
        {
            wheelRotation -= 360;
        }
        Debug.Log(steering * maxSteeringAngle);
        if (Throttle.axis.magnitude != 0) { 
            foreach (AxleInfo axleInfo in axleInfos)
            {
                if (axleInfo.steering)
                {
                    axleInfo.leftWheel.steerAngle = steering * maxSteeringAngle;
                    axleInfo.rightWheel.steerAngle = steering * maxSteeringAngle;
                }
                if (axleInfo.motor)
                {
                    axleInfo.leftWheel.motorTorque = Throttle.axis.y * maxMotorTorque;
                    axleInfo.rightWheel.motorTorque = Throttle.axis.y * maxMotorTorque;
                }
            }
        }
        else if(UnityEngine.Input.GetAxis("Vertical") != 0) {
            Debug.Log(steeringa * maxSteeringAngle);
            foreach (AxleInfo axleInfo in axleInfos)
            {
                if (axleInfo.steering)
                {
                    axleInfo.leftWheel.steerAngle = (steeringa) ;
                    axleInfo.rightWheel.steerAngle = (steeringa);
                }
                if (axleInfo.motor)
                {
                    axleInfo.leftWheel.motorTorque = motor;
                    axleInfo.rightWheel.motorTorque = motor;
                }
            }
        }
        else
        {
            foreach (AxleInfo axleInfo in axleInfos)
            {
                if (axleInfo.motor) { 
                    axleInfo.leftWheel.motorTorque = 0;
                    axleInfo.rightWheel.motorTorque = 0;
                }
            }
        }
    }
}

[System.Serializable]
public class AxleInfo
{
    public WheelCollider leftWheel;
    public WheelCollider rightWheel;
    public bool motor; // is this wheel attached to motor?
    public bool steering; // does this wheel apply steer angle?
}