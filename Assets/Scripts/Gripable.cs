﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class Gripable : MonoBehaviour
{



    public delegate void OnAttachedToHandDelegate(Hand hand);
    public delegate void OnDetachedFromHandDelegate(Hand hand);

    public event OnAttachedToHandDelegate onAttachedToHand;
    public event OnDetachedFromHandDelegate onDetachedFromHand;

    public Hand.AttachmentFlags attachmentFlags = Hand.AttachmentFlags.ParentToHand | Hand.AttachmentFlags.DetachFromOtherHand | Hand.AttachmentFlags.TurnOnKinematic;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    }

    protected void HandHoverUpdate(Hand hand)
    {
        GrabTypes startingGrabType = hand.GetGrabStarting();
        if(startingGrabType == GrabTypes.Grip) { 
            hand.AttachObject(gameObject, startingGrabType, attachmentFlags, null);
        }
    }
    protected void HandAttachedUpdate(Hand hand)
    {
        Debug.Log("Attached to hand");
    }
    protected virtual void OnAttachedToHand(Hand hand)
    {
        Debug.Log("Attached to hand");
    }
}